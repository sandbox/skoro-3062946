Userpoints Disqus
=================

This module allows the user to earn points on posting Disqus comments.

Install
=======

The [Disqus](https://www.drupal.org/project/disqus) module of version **1.0-rc3** needs a patch!
Get the patch from [here](https://www.drupal.org/project/disqus/issues/2931745).
After that you can install this module. Without the patch this
module won't work.

More details: this module depends on `#callback` feature of the Disqus form element.
But foreign callbacks don't work out of the box. The patch solves callbacks and I
hope that it will be integrated in the future in the Disqus module. 

Configure
=========

No special configuration for this module is needed.
You need to ensure that the Disqus credentials are provided:
  * Secret key
  * Public key
  * User access token
  
Register your application and get these credentials on the [applications](https://disqus.com/api/applications/) page.

By default, user points on disqus comments are disabled.
To enable them go to:
**Administration -> Configuration -> Web services -> Disqus**
and set the **Enable user points callback** checkbox and after that clear the cache.

Implementation
==============

This module is implemented as a reaction [Rule](https://www.drupal.org/project/rules). Each time as a
new comment is posted that rule is fired. It creates the
transaction, sets the amount of points and stores a comment
text in the transaction.

Because the transactions operate only on entites there is only
a special field `field_disqus_comment` for storing a disqus comment text in the transaction.

If you want to change amount of points (by default 1 point) for comment you need to
edit `earn_points_for_disqus_comment` rule. In that rule open
`Action: Create a new User points transaction` and in the **AMOUNT** section
set your desired value and save the action and the rule.
