/**
 * @file
 * JavaScript for the Userpoints Disqus module.
 */

(function ($) {

Drupal.userpoints = Drupal.userpoints || {};

Drupal.userpoints.disqusNewComment = function (comment) {
  $.ajax({
    url: '/userpoints_disqus/newcomment',
    method: 'POST',
    data: {
      id: comment.id,
      text: comment.text,
      // FIXME: the current url is worth to get here but I don't know how to
      // FIXME: get a comment url from the Disqus API.
      url: ''
    }
  });
};

})(jQuery);