<?php

namespace Drupal\userpoints_disqus\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\userpoints_disqus\DisqusComment;
use Drupal\userpoints_disqus\Event\DisqusCommentEvent;
use Drupal\userpoints_disqus\ValidCommentException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Accepts a new Disqus comment and creates an event for the Rules module.
 */
class NewCommentController extends ControllerBase {

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Controller constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('event_dispatcher')
    );
  }

  /**
   * Dispatches a new comment event.
   *
   * @param \Drupal\userpoints_disqus\DisqusComment $comment
   *   The Disqus comment model.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @throws \RuntimeException
   *   When a user entity cannot be found.
   */
  protected function dispatchNewCommentEvent(DisqusComment $comment) {
    $account = $this->currentUser();
    $user = $this->entityTypeManager()
      ->getStorage('user')
      ->load($account->id());
    if (empty($user)) {
      throw new \RuntimeException("Couldn't load user: " . $account->id());
    }
    $event = new DisqusCommentEvent($user, $comment);
    $this->eventDispatcher->dispatch(DisqusCommentEvent::EVENT_NAME, $event);
  }

  /**
   * Accepts a new disqus comment.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The http request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The status of operation. Returns a json with the following fields:
   *   - "status" (bool) the status
   *   - "message" (string) the error message, only for FALSE statuses.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   *   When a new comment posting is disabled.
   */
  public function accept(Request $request): Response {

    $config = $this->config('userpoints_disqus.settings');
    if (!$config->get('userpoints_newcomment')) {
      throw new NotFoundHttpException();
    }

    $response = ['status' => TRUE];

    try {
      $comment_id = $request->get('id');
      if (empty($comment_id)) {
        throw new \InvalidArgumentException('comment is required.');
      }

      if (!preg_match('/^[0-9]+$/', $comment_id)) {
        throw new \InvalidArgumentException('comment.id is required.');
      }

      $text = $request->get('text');
      $comment = new DisqusComment($comment_id, $text, '');

      $this->validateComment($comment);

      $this->dispatchNewCommentEvent($comment);
    }
    catch (\Exception $e) {
      $response['status'] = FALSE;
      $response['message'] = $e->getMessage();
    }

    return new JsonResponse($response);
  }

  /**
   * Validates a comment against the Disqus API.
   *
   * @param DisqusComment $comment
   *   The Disqus comment model.
   *
   * @throws \Drupal\userpoints_disqus\ValidCommentException
   * @throws \DisqusAPIError
   */
  protected function validateComment(DisqusComment $comment) {
    $api = disqus_api();
    $details = $api->posts->details([
      'post' => $comment->getId(),
    ]);
    if ($details->isApproved === TRUE && $details->isDeleted === FALSE &&
        $details->raw_message === $comment->getText()) {
      return;
    }
    throw new ValidCommentException($comment);
  }

}
