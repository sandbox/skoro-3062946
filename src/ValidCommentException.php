<?php

namespace Drupal\userpoints_disqus;

/**
 * The exception happens when a comment isn't valid.
 */
class ValidCommentException extends \Exception {

  /**
   * @var \Drupal\userpoints_disqus\DisqusComment
   */
  private $comment;

  public function __construct(DisqusComment $comment) {
    parent::__construct("Couldn't validate your comment in the Disqus API.");
    $this->comment = $comment;
  }

  public function getComment(): DisqusComment {
    return $this->comment;
  }

}
