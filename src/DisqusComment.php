<?php

namespace Drupal\userpoints_disqus;

/**
 * Disqus comment model.
 */
class DisqusComment {

  /**
   * The comment id.
   *
   * @var int
   */
  private $id;

  /**
   * The comment text.
   *
   * @var string
   */
  private $text;

  /**
   * The article url.
   *
   * @var string
   */
  private $url;

  /**
   * DisqusComment constructor.
   *
   * @param int $id
   *   The comment ID.
   * @param string $text
   *   The comment text.
   * @param string $url
   *   The article url.
   *
   * @throws \InvalidArgumentException
   */
  public function __construct(int $id, string $text, string $url) {
    $this->id = $id;
    $text = trim($text);
    if (empty($text)) {
      throw new \InvalidArgumentException('Comment text is required and cannot be empty.');
    }
    $this->text = $text;
    $this->url = $url;
  }

  /**
   * Returns the comment id.
   *
   * @return int
   *   The comment id.
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * Returns the comment text.
   *
   * @return string
   *   The comment text.
   */
  public function getText(): string {
    return $this->text;
  }

  /**
   * Returns the article url.
   *
   * @return string
   *   The article url.
   */
  public function getUrl(): string {
    return $this->url;
  }

  /**
   * Converts the comment into a string.
   *
   * @return string
   *   The string presentation.
   */
  public function __toString() {
    return $this->text;
  }

}