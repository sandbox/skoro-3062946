<?php

namespace Drupal\userpoints_disqus\Event;

use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\userpoints_disqus\DisqusComment;

/**
 * Class DisqusCommentEvent
 *
 * @package Drupal\userpoints_disqus\Event
 */
class DisqusCommentEvent extends Event {

  const EVENT_NAME = 'userpoints_disqus_newcomment';

  /**
   * The user account.
   *
   * @var \Drupal\user\UserInterface
   */
  public $account;

  /**
   * The Disqus comment model.
   *
   * @var \Drupal\userpoints_disqus\DisqusComment
   */
  public $comment;

  /**
   * Constructs the object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account of the user logged in.
   * @param \Drupal\userpoints_disqus\DisqusComment $comment
   *   The Disqus comment model.
   */
  public function __construct(UserInterface $account, DisqusComment $comment) {
    $this->account = $account;
    $this->comment = $comment;
  }

}
